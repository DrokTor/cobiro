import { Component, OnInit } from '@angular/core';
import logo from '../../assets/logo.svg';
import arrow from '../../assets/arrow.svg';
import niceBeautyImg from '../../assets/nicebeauty.com_eu_.png';
import profile from '../../assets/profile.png';


interface item {
  id:	number;
  title:	string;
  parent_id: number | null;
  width?: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    fetch('http://localhost:3000/items')
    .then(resp => resp.json())
    .then(data => {
      this.data = [ ...data ];
      this.data = data.map((it: item) => {
        if(it.parent_id !== null){
          return { ...it, width: (100-(this.getParentWidth(it)+3))+'%' };
        }else{
          return it;
        }
      })
      this.items = this.data;
    })
    .catch(reason => console.log(reason));
  }

  getParentWidth(item: item): number{
      if(item.parent_id === null){
        return 0;
      }else{
        const parent: item  = this.data.find((it: item) => it.id === item.parent_id );
        return this.getParentWidth(parent)+3;
      }
  }

  search(e: string) {
    this.items = this.data.filter((it: item) => it.title.match(new RegExp( ".*"+e+".*", 'i' )) );
  }

  data: item[] = []

  items: item[]

  logo = logo
  arrow = arrow
  niceBeautyImg = niceBeautyImg
  profile = profile

  menuItems: string[] = [
    'Marketing Plan',
    'Dashboard',
    'My Ads',
    'Analytics',
    'Cobiro Store',
  ]
}
