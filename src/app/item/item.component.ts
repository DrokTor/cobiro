import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import niceBeautyImg from '../../assets/nicebeauty.com_eu_.png';

interface Item {
  id:	number;
  title:	string;
  parent_id: number | null;
  parent?: string;
}

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass']
})
export class ItemComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      fetch('http://localhost:3000/items/'+params.get('id'))
      .then(resp => resp.json())
      .then(data => {
        if(data.parent_id !== null){
          fetch('http://localhost:3000/items/'+data.parent_id)
          .then(resp => resp.json())
          .then(data => {
            this.item.parent = data.title
          })
        }
        this.item = data;
        console.log(this.item)
      })
      .catch(reason => console.log(reason));
    });
  }

  niceBeautyImg = niceBeautyImg

  item: Item

}
