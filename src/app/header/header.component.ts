import { Component, OnInit } from '@angular/core';
import logo from '../../assets/logo.svg';
import arrow from '../../assets/arrow.svg';
import niceBeautyImg from '../../assets/nicebeauty.com_eu_.png';
import profile from '../../assets/profile.png';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }


  logo = logo
  arrow = arrow
  niceBeautyImg = niceBeautyImg
  profile = profile

}
