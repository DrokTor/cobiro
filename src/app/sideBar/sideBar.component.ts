import { Component, OnInit } from '@angular/core';
import niceBeautyImg from '../../assets/nicebeauty.com_eu_.png';


@Component({
  selector: 'app-sideBar',
  templateUrl: './sideBar.component.html',
  styleUrls: ['./sideBar.component.sass']
})
export class SideBarComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }
  
  niceBeautyImg = niceBeautyImg

  menuItems: string[] = [
    'Marketing Plan',
    'Dashboard',
    'My Ads',
    'Analytics',
    'Cobiro Store',
  ]

}
